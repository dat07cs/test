package com.company;

import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        Integer a = null;
        System.out.println(a == 0);


        Object aa = new Object();
        System.out.println(aa);
        Object bb = aa;
        Object cc = new Object();
        aa = cc;
        System.out.println(aa);
        System.out.println(bb);
        System.out.println(cc);
        List<String> strings = new ArrayList<String>(Arrays.asList(null, "aaa", "bbb", "ccc"));
        System.out.println(Arrays.toString(strings.toArray(new String[0])));
        /*double[] aa = adjustLineValues(80000, 70000, 70000, 70000, 50000, 70000, 60000, 10000, 69000, 70000, 40000, 60000, 50000, 68000);
        for (double a : aa) {
            System.out.println(a);
        }*/
        //System.out.println(new DecimalFormat("#,##0").format(123));
        DateTime todayDateTime = DateTime.now().withTime(0, 0, 0, 0);
        Date today = todayDateTime.toDate();
        Date firstDateOfMonth = todayDateTime.withDayOfMonth(1).toDate();
        Date lastDateOfMonth = todayDateTime.withDayOfMonth(todayDateTime.dayOfMonth().getMaximumValue()).toDate();
        System.out.println(today);
        System.out.println(firstDateOfMonth);
        System.out.println(lastDateOfMonth);

        String codes = "   ,  ,  , ,  abc , ,  , , , , def , ,, ,  ,";
        String[] split = codes.replaceAll("[,\\s]+", " ").trim().toUpperCase().split(" ");
        for (String str : split) {
            System.out.print(str + "_");
        }
        List<Object> d = new ArrayList<Object>(0);
        d.add(1);
        BigDecimal ZERO = new BigDecimal("0.000000");
        System.out.println(ZERO.equals(BigDecimal.valueOf(0, 1).setScale(6, RoundingMode.HALF_DOWN)));
        System.out.println(doTrue() || doFalse() && doTrue());
        System.out.println(doTrue() || doTrue() && doTrue());
        System.out.println(doTrue() || doFalse() && doFalse());
        System.out.println(doTrue() || doTrue() && doFalse());
        System.out.println(doFalse() || doFalse() && doTrue());
        System.out.println(doFalse() || doTrue() && doTrue());
        System.out.println(doFalse() || doFalse() && doFalse());
        System.out.println(doFalse() || doTrue() && doFalse());
        System.out.println(doTrue() && doFalse() || doTrue());
        System.out.println(doTrue() && doTrue() || doTrue());
        System.out.println(doTrue() && doFalse() || doFalse());
        System.out.println(doTrue() && doTrue() || doFalse());
        System.out.println(doFalse() && doFalse() || doTrue());
        System.out.println(doFalse() && doTrue() || doTrue());
        System.out.println(doFalse() && doFalse() || doFalse());
        System.out.println(doFalse() && doTrue() || doFalse());

        System.out.println(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS Z").format(new Date(100, 00, 01, 10, 10, 10)));
    }

    private static void overload(int a, long b) {
        System.out.println("overload(int a, long b)");
    }

    private static void overload(int a, Integer b) {
        System.out.println("overload(int a, Integer b)");
    }

    private static void overload(long a, int b) {
        System.out.println("overload(long a, int b)");
    }

    private static void overload(Integer a, int b) {
        System.out.println("overload(Integer a, int b)");
    }

    private static boolean doFalse() {
        System.out.println("run false");
        return false;
    }
    private static boolean doTrue() {
        System.out.println("run true");
        return true;
    }

    private static double[] adjustLineValues(double ymax, double...values) {
        if (values.length < 2) {
            return values;
        }

        double adjustValue = ymax / 200;
        double[] adjustedValues = new double[values.length];
        Map<Double, List<Integer>> map = new HashMap<Double, List<Integer>>();
        for (int i = 0; i < values.length; i++) {
            if (map.get(values[i]) == null) {
                map.put(values[i], new ArrayList<Integer>());
            }
            map.get(values[i]).add(i);
        }

        List<Double> keys = new ArrayList<Double>(map.keySet());
        Collections.sort(keys);
        int i = keys.size() - 1;
        Double pilot = null;
        do {
            double value = keys.get(i);
            List<Integer> positions = map.get(value);
            for (int position : positions) {
                if (pilot == null) {
                    pilot = value;
                } else {
                    pilot = Math.min(value, pilot - adjustValue);
                }
                adjustedValues[position] = pilot;
            }
            i--;
        } while (i >= 0);

        return adjustedValues;
    }
}
