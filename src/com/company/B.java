package com.company;

/**
 * @author Dat Le <dat.le@gbst.com>
 */
public class B extends A {
    @Override
    public B get(B b) throws ClassCastException {
        return b;
    }
}
